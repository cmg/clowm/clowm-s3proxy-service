# 🚨 Deprecation 🚨
This repository was merged into the [CloWM backend](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-backend) and will NOT be developed further.  
Everything is in read-only mode.

# CloWM S3Proxy Service

## Description

Openstack is shipping with an integrated UI to access the Object Store provided by Ceph. Unfortunately, this UI does not
allow fine-grained control who can access a bucket or object. You can either make it accessible for everyone or nobody,
but Ceph can do this and much more. 👎
This is the backend for a new UI which can leverage the additional powerful functionality provided by Ceph in a
user-friendly manner. 👍

| Feature                     | Openstack Integration | New UI |
|-----------------------------|:---------------------:|:------:|
| Create / Delete Buckets UI  |           ✅           |   ✅    |
| Create / Delete Buckets CLI |           ✅           |   ❌    |
| Upload / Download Objects   |           ✅           |   ✅    |
| Fine-grained Access Control |           ❌           |   ✅    |

### Concept

![Visualization of Concept](figures/cloud_object_storage.svg)

## Configuration

### General

| Env variable             | Config file key | Default       | Value    | Example                | Description                                                                                                           |
|--------------------------|-----------------|---------------|----------|------------------------|-----------------------------------------------------------------------------------------------------------------------|
| `CLOWM_CONFIG_FILE_YAML` | -               | `config.yaml` | Filepath | `/path/to/config.yaml` | Path to a YAML file to read the config. See [example-config/example-config.yaml](example-config/example-config.yaml). |
| `CLOWM_CONFIG_FILE_TOML` | -               | `config.toml` | Filepath | `/path/to/config.toml` | Path to a TOML file to read the config. See [example-config/example-config.toml](example-config/example-config.toml). |
| `CLOWM_CONFIG_FILE_JSON` | -               | `config.json` | Filepath | `/path/to/config.json` | Path to a JSON file to read the config. See [example-config/example-config.json](example-config/example-config.json). |
| `CLOWM_API_PREFIX`       | `api_prefix`    | unset         | URI path | `/api`                 | Prefix before every URL path                                                                                          |
| * `CLOWM_UI_URI`         | `ui_uri`        | unset         | HTTP URL | `https://localhost`    | HTTP URL of the CloWM website                                                                                         |

### Database

| Env variable           | Config file key | Default     | Value              | Example       | Description                                                    |
|------------------------|-----------------|-------------|--------------------|---------------|----------------------------------------------------------------|
| `CLOWM_DB__HOST`       | `db.host`       | `localhost` | <db hostname / IP> | `localhost`   | IP or Hostname Address of DB                                   |
| `CLOWM_DB__PORT`       | `db.port`       | 3306        | Integer            | 3306          | Port of the database                                           |
| * `CLOWM_DB__USER`     | `db.user`       | unset       | String             | `db-user`     | Username of the database user                                  |
| * `CLOWM_DB__PASSWORD` | `db.password`   | unset       | String             | `db-password` | Password of the database user                                  |
| * `CLOWM_DB__NAME`     | `db.name`       | unset       | String             | `db-name`     | Name of the database                                           |
| `CLOWM_DB__VERBOSE`    | `db.verbose`    | `false`     | Boolean            | `false`       | Enables verbose SQL output.<br>Should be `false` in production |

### S3

| Env variable                            | Config file key                  | Default   | Value    | Example                    | Description                                                                      |
|-----------------------------------------|----------------------------------|-----------|----------|----------------------------|----------------------------------------------------------------------------------|
| * `CLOWM_S3__URI`                       | `s3.uri`                         | unset     | HTTP URL | `http://localhost`         | URI of the S3 Object Storage                                                     |
| * `CLOWM_S3__ACCESS_KEY`                | `s3.acess_key`                   | unset     | String   | `ZR7U56KMK20VW`            | Access key for the S3 that owns the buckets                                      |
| * `CLOWM_S3__SECRET_KEY`                | `s3.secret_key`                  | unset     | String   | `9KRUU41EGSCB3H9ODECNHW`   | Secret key for the S3 that owns the buckets                                      |
| * `CLOWM_S3__USERNAME`                  | `s3.username`                    | unset     | String   | `clowm-bucket-manager`     | ID of the user in ceph who owns all the buckets. Owner of `CLOWM_S3__ACCESS_KEY` |
| * `CLOWM_S3__ADMIN_ACCESS_KEY`          | `s3.admin_acess_key`             | unset     | String   | `ZR7U56KMK20VW`            | Access key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities. |
| * `CLOWM_S3__ADMIN_SECRET_KEY`          | `s3.admin_secret_key`            | unset     | String   | `9KRUU41EGSCB3H9ODECNHW`   | Secret key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities. |
| `CLOWM_S3__DEFAULT_BUCKET_SIZE_LIMIT`   | `s3.default_bucket_size_limit`   | `400 GiB` | ByteSize | `10 KB`, `10 KiB`, `10 MB` | Size limit of a new Bucket. Between `1 KiB` and `4.3 TB`                         |
| `CLOWM_S3__DEFAULT_BUCKET_OBJECT_LIMIT` | `s3.default_bucket_object_limit` | `40000`   | Integer  | `10000`                    | Maximum number of objects in a new bucket. Must be $<2^{32}$                     |

### Security

| Env variable                                   | Config file key                  | Default | Value                           | Example            | Description                                         |
|------------------------------------------------|----------------------------------|---------|---------------------------------|--------------------|-----------------------------------------------------|
| * `CLOWM_PUBLIC_KEY` / `CLOWM_PUBLIC_KEY_FILE` | `public_key` / `public_key_file` | unset   | Public Key / Path to Public Key | `/path/to/key.pub` | Public part of RSA Key in PEM format to verify JWTs |
| * `CLOWM_OPA__URI`                             | `opa.uri`                        | unset   | HTTP URL                        | `http://localhost` | URI of the OPA Service                              |

### Monitoring

| Env variable                | Config file key      | Default | Value   | Example     | Description                                                                                  |
|-----------------------------|----------------------|---------|---------|-------------|----------------------------------------------------------------------------------------------|
| `CLOWM_OTLP__GRPC_ENDPOINT` | `otlp.grpc_endpoint` | unset   | String  | `localhost` | OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger. If unset, no traces are sent. |
| `CLOWM_OTLP__SECURE`        | `otlp.secure`        | `false` | Boolean | `false`     | Connection type                                                                              |

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information.

## Version 1.1.2

### General
* When requesting a list of buckets, all buckets, including WRITE-only buckets, are returned #34

### Fixes
* User with WRITE/READWRITE permission can now delete multiple Objects with a single request to the S3 endpoint #34

## Version 1.1.1

### General
* Show correct version number in the Swagger UI #33
* Fix typos in README & DEVELOPING

## Version 1.1.0

### Features
* Inject additional metadata to `BucketOut` and `S3ObjectMetaInformation` response #22
* Add name of user in the `BucketPermissionOut` response #29
* Add endpoint to search for user #28
* Enforce that always a S3 Key is present #27
### Fixes
* Use `max-age` for cookies instead of `expires` #32
* Give useful response when encountering a error during login instead of sending a `500` response #21
* Remove dependence on other services during health check #31
### General
* Bump dependencies
* Bump version of dev OIDC service and adapt configuration #24

import json
from typing import Annotated, Any, Awaitable, Callable

from clowmdb.models import BucketPermission
from fastapi import APIRouter, Body, Depends, HTTPException, Query, status
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema

from app.api.dependencies import (
    AuthorizationDependency,
    CurrentBucket,
    CurrentUser,
    DBSession,
    PathUser,
    S3Resource,
    get_current_bucket,
    get_user_by_path_uid,
)
from app.ceph.s3 import get_s3_bucket_policy, put_s3_bucket_policy
from app.crud import CRUDBucketPermission, DuplicateError
from app.otlp import start_as_current_span_async
from app.schemas.bucket_permission import BucketPermissionIn, BucketPermissionOut, BucketPermissionParameters

router = APIRouter(prefix="/permissions", tags=["BucketPermission"])
permission_authorization = AuthorizationDependency(resource="bucket_permission")
Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(permission_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get(
    "",
    response_model=list[BucketPermissionOut],
    summary="Get all permissions.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_list_bucket_permission", tracer=tracer)
async def list_permissions(
    db: DBSession,
    authorization: Authorization,
    permission_types: Annotated[
        list[BucketPermission.Permission] | SkipJsonSchema[None],
        Query(description="Type of Bucket Permissions to fetch"),
    ] = None,
    permission_status: Annotated[
        CRUDBucketPermission.PermissionStatus | SkipJsonSchema[None],
        Query(description="Status of Bucket Permissions to fetch"),
    ] = None,
) -> list[BucketPermissionOut]:
    """
    List all the bucket permissions in the system.\n
    Permission `bucket_permission:list_all` required.
    \f
    Parameters
    ----------
    permission_types : list[clowmdb.models.BucketPermission.Permission] | None, default None
        Type of Bucket Permissions to fetch. Query Parameter
    permission_status : app.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
        Status of Bucket Permissions to fetch. Query Parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    permissions : list[app.schemas.bucket_permission.BucketPermissionOut]
        List of all permissions.
    """
    current_span = trace.get_current_span()
    if permission_types is not None and len(permission_types) > 0:  # pragma: no cover
        current_span.set_attribute("permission_types", [ptype.name for ptype in permission_types])
    if permission_status is not None:  # pragma: no cover
        current_span.set_attribute("permission_status", permission_status.name)
    rbac_operation = "list_all"
    await authorization(rbac_operation)
    bucket_permissions = await CRUDBucketPermission.list(
        db=db, permission_types=permission_types, permission_status=permission_status
    )
    return [BucketPermissionOut.from_db_model(p) for p in bucket_permissions]


@router.post(
    "",
    response_model=BucketPermissionOut,
    status_code=status.HTTP_201_CREATED,
    summary="Create a permission.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_create_bucket_permission", tracer=tracer)
async def create_permission(
    db: DBSession,
    current_user: CurrentUser,
    s3: S3Resource,
    authorization: Authorization,
    permission: Annotated[BucketPermissionIn, Body(..., description="Permission to create")],
) -> BucketPermissionOut:
    """
    Create a permission for a bucket and user.\n
    Permission `bucket_permission:create` required.
    \f
    Parameters
    ----------
    permission : app.schemas.bucket_permission.BucketPermissionOut
        Information about the permission which should be created. HTTP Body parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    permissions : app.schemas.bucket_permission.BucketPermissionOut
        Newly created permission.
    """
    current_span = trace.get_current_span()
    current_span.set_attributes({"uid": str(permission.uid), "bucket_name": permission.bucket_name})
    await authorization("create")
    target_bucket = await get_current_bucket(permission.bucket_name, db=db)  # Check if the target bucket exists
    if target_bucket.owner_id != current_user.uid:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Action forbidden.")
    await get_user_by_path_uid(permission.uid, db)  # Check if target user exists
    try:
        permission_db = await CRUDBucketPermission.create(permission, db=db)
    except ValueError as e:
        current_span.record_exception(e)
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST, detail="The owner of the bucket can't get any more permissions"
        )
    except DuplicateError as e:
        current_span.record_exception(e)
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            detail=f"Permission for combination of bucket={permission.bucket_name} and user={permission.uid} already exists",  # noqa:E501
        )
    s3_policy = get_s3_bucket_policy(s3, bucket_name=permission.bucket_name)
    json_policy = json.loads(s3_policy.policy)
    json_policy["Statement"] += permission.map_to_bucket_policy_statement(permission_db.uid)
    put_s3_bucket_policy(s3, bucket_name=permission.bucket_name, policy=json.dumps(json_policy))

    return BucketPermissionOut.from_db_model(permission_db)


@router.get(
    "/user/{uid}",
    response_model=list[BucketPermissionOut],
    summary="Get all permissions for a user.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_list_bucket_permission_for_user", tracer=tracer)
async def list_permissions_per_user(
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
    permission_types: Annotated[
        list[BucketPermission.Permission] | SkipJsonSchema[None],
        Query(description="Type of Bucket Permissions to fetch"),
    ] = None,
    permission_status: Annotated[
        CRUDBucketPermission.PermissionStatus | SkipJsonSchema[None],
        Query(description="Status of Bucket Permissions to fetch"),
    ] = None,
) -> list[BucketPermissionOut]:
    """
    List all the bucket permissions for the given user.\n
    Permission `bucket_permission:list_user` required if current user is the target the bucket permission,
    otherwise `bucket_permission:list_all` required.
    \f
    Parameters
    ----------
    permission_types : list[clowmdb.models.BucketPermission.Permission] | None, default None
        Type of Bucket Permissions to fetch. Query Parameter
    permission_status : app.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
        Status of Bucket Permissions to fetch. Query Parameter.
    user : clowmdb.models.User
        User with given uid. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    permissions : list[app.schemas.bucket_permission.BucketPermissionOut]
        List of all permissions for this user.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("uid", str(user.uid))
    if permission_types is not None and len(permission_types) > 0:  # pragma: no cover
        current_span.set_attribute("permission_types", [ptype.name for ptype in permission_types])
    if permission_status is not None:  # pragma: no cover
        current_span.set_attribute("permission_status", permission_status.name)
    rbac_operation = "list_user" if user == current_user else "list_all"
    await authorization(rbac_operation)
    bucket_permissions = await CRUDBucketPermission.list(
        db=db, uid=user.uid, permission_types=permission_types, permission_status=permission_status
    )
    return [BucketPermissionOut.from_db_model(p) for p in bucket_permissions]


@router.get(
    "/bucket/{bucket_name}",
    response_model=list[BucketPermissionOut],
    summary="Get all permissions for a bucket.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_list_bucket_permission_for_bucket", tracer=tracer)
async def list_permissions_per_bucket(
    bucket: CurrentBucket,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    permission_types: Annotated[
        list[BucketPermission.Permission] | SkipJsonSchema[None],
        Query(description="Type of Bucket Permissions to fetch"),
    ] = None,
    permission_status: Annotated[
        CRUDBucketPermission.PermissionStatus | SkipJsonSchema[None],
        Query(description="Status of Bucket Permissions to fetch"),
    ] = None,
) -> list[BucketPermissionOut]:
    """
    List all the bucket permissions for the given bucket.\n
    Permission `bucket_permission:list_bucket` required if current user is owner of the bucket,
    otherwise `bucket_permission:list_all` required.
    \f
    Parameters
    ----------
    permission_types : list[clowmdb.models.BucketPermission.Permission] | None, default None
        Type of Bucket Permissions to fetch. Query Parameter
    permission_status : app.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
        Status of Bucket Permissions to fetch. Query Parameter.
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    permissions : list[app.schemas.bucket_permission.BucketPermissionOut]
        List of all permissions for this bucket.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    if permission_types is not None and len(permission_types) > 0:  # pragma: no cover
        current_span.set_attribute("permission_types", [ptype.name for ptype in permission_types])
    if permission_status is not None:  # pragma: no cover
        current_span.set_attribute("permission_status", permission_status.name)
    rbac_operation = "list_bucket" if bucket.owner_id == current_user.uid else "list_all"
    await authorization(rbac_operation)
    bucket_permissions = await CRUDBucketPermission.list(
        db=db, bucket_name=bucket.name, permission_types=permission_types, permission_status=permission_status
    )
    return [BucketPermissionOut.from_db_model(p) for p in bucket_permissions]


@router.get(
    "/bucket/{bucket_name}/user/{uid}",
    response_model=BucketPermissionOut,
    summary="Get permission for bucket and user combination.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_get_bucket_permission", tracer=tracer)
async def get_permission_for_bucket(
    bucket: CurrentBucket,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> BucketPermissionOut:
    """
    Get the bucket permissions for the specific combination of bucket and user.\n
    The owner of the bucket and the grantee of the permission can view it.\n
    Permission `bucket_permission:read` required if current user is the target or owner of the bucket permission,
    otherwise `bucket_permission:read_any` required.
    \f
    Parameters
    ----------
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    user : clowmdb.models.User
        User with the uid in the URL. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    permissions : app.schemas.bucket_permission.BucketPermissionOut
        Permission for this bucket and user combination.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "uid": str(user.uid)})
    rbac_operation = "read" if user == current_user or current_user.uid == bucket.owner_id else "read_any"
    await authorization(rbac_operation)
    bucket_permission = await CRUDBucketPermission.get(bucket.name, user.uid, db=db)
    if bucket_permission:
        return BucketPermissionOut.from_db_model(bucket_permission)
    raise HTTPException(
        status.HTTP_404_NOT_FOUND,
        detail=f"Permission for combination of bucket={bucket.name} and user={user.uid} doesn't exists",
    )


@router.delete(
    "/bucket/{bucket_name}/user/{uid}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete a bucket permission",
)
@start_as_current_span_async("api_delete_bucket_permission", tracer=tracer)
async def delete_permission(
    bucket: CurrentBucket,
    db: DBSession,
    s3: S3Resource,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> None:
    """
    Delete the bucket permissions for the specific combination of bucket and user.\n
    The owner of the bucket and the grantee of the permission can delete it.\n
    Permission `bucket_permission:delete` required if current user is the target or owner of the bucket permission,
    otherwise `bucket_permission:delete_any` required.
    \f
    Parameters
    ----------
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    user : clowmdb.models.User
        User with the uid in the URL. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    permissions : app.schemas.bucket_permission.BucketPermissionOut
        Permission for this bucket and user combination.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "uid": str(user.uid)})
    rbac_operation = "delete" if user == current_user or current_user.uid == bucket.owner_id else "delete_any"
    await authorization(rbac_operation)
    bucket_permission = await CRUDBucketPermission.get(bucket.name, user.uid, db=db)
    if bucket_permission is None:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail=f"Permission for combination of bucket={bucket.name} and user={str(user.uid)} doesn't exists",
        )
    await CRUDBucketPermission.delete(db=db, bucket_name=bucket_permission.bucket_name, uid=bucket_permission.uid)
    bucket_permission_schema = BucketPermissionOut.from_db_model(bucket_permission)
    s3_policy = get_s3_bucket_policy(s3, bucket_name=bucket_permission_schema.bucket_name)
    policy = json.loads(s3_policy.policy)
    policy["Statement"] = [
        stmt for stmt in policy["Statement"] if stmt["Sid"] != bucket_permission_schema.to_hash(user.uid)
    ]
    put_s3_bucket_policy(s3, bucket_name=bucket_permission_schema.bucket_name, policy=json.dumps(policy))


@router.put(
    "/bucket/{bucket_name}/user/{uid}",
    status_code=status.HTTP_200_OK,
    response_model=BucketPermissionOut,
    summary="Update a bucket permission",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_create_bucket_permission", tracer=tracer)
async def update_permission(
    bucket: CurrentBucket,
    db: DBSession,
    current_user: CurrentUser,
    s3: S3Resource,
    authorization: Authorization,
    user: PathUser,
    permission_parameters: Annotated[BucketPermissionParameters, Body(..., description="Permission to create")],
) -> BucketPermissionOut:
    """
    Update a permission for a bucket and user.\n
    Permission `bucket_permission:update` required.
    \f
    Parameters
    ----------
    permission_parameters : app.schemas.bucket_permission.BucketPermissionOut
        Information about the permission which should be updated. HTTP Body parameter.
    user : clowmdb.models.User
        User with the uid in the URL. Dependency Injection.
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    permissions : app.schemas.bucket_permission.BucketPermissionOut
        Updated permission.
    """
    trace.get_current_span().set_attributes({"uid": str(user.uid), "bucket_name": bucket.name})
    await authorization("update")
    if bucket.owner_id != current_user.uid:
        raise HTTPException(status.HTTP_403_FORBIDDEN, "Action forbidden")
    bucket_permission = await CRUDBucketPermission.get(bucket.name, user.uid, db=db)

    if bucket_permission is None:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail=f"Permission for combination of bucket={bucket.name} and user={user.uid} doesn't exists",
        )
    updated_permission = await CRUDBucketPermission.update_permission(bucket_permission, permission_parameters, db=db)
    updated_permission_schema = BucketPermissionOut.from_db_model(updated_permission)
    s3_policy = get_s3_bucket_policy(s3, bucket_name=bucket.name)
    policy = json.loads(s3_policy.policy)
    policy["Statement"] = [
        stmt for stmt in policy["Statement"] if stmt["Sid"] != updated_permission_schema.to_hash(user.uid)
    ]
    policy["Statement"] += updated_permission_schema.map_to_bucket_policy_statement(updated_permission.uid)
    put_s3_bucket_policy(s3, bucket_name=bucket.name, policy=json.dumps(policy))
    return updated_permission_schema

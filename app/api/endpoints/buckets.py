import json
from typing import Annotated, Any, Awaitable, Callable, Iterable
from uuid import UUID

from botocore.exceptions import ClientError
from clowmdb.models import Bucket
from fastapi import APIRouter, Body, Depends, HTTPException, Query, status
from opentelemetry import trace
from pydantic import ByteSize
from pydantic.json_schema import SkipJsonSchema

from app.api.dependencies import (
    AuthorizationDependency,
    CurrentBucket,
    CurrentUser,
    DBSession,
    RGWAdminResource,
    S3Resource,
)
from app.ceph.rgw import update_bucket_limits as rgw_update_bucket_limits
from app.ceph.s3 import get_s3_bucket_objects, get_s3_bucket_policy, put_s3_bucket_policy
from app.core.config import settings
from app.crud import CRUDBucket, CRUDBucketPermission, DuplicateError
from app.otlp import start_as_current_span_async
from app.schemas.bucket import BucketIn as BucketInSchema
from app.schemas.bucket import BucketOut as BucketOutSchema
from app.schemas.bucket import BucketSizeLimits

router = APIRouter(prefix="/buckets", tags=["Bucket"])
bucket_authorization = AuthorizationDependency(resource="bucket")
Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(bucket_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)
ANONYMOUS_ACCESS_SID = "AnonymousAccess"

cors_rule = {
    "CORSRules": [
        {
            "ID": "websiteaccess",
            "AllowedHeaders": [
                "amz-sdk-invocation-id",
                "amz-sdk-request",
                "authorization",
                "content-type",
                "x-amz-content-sha256",
                "x-amz-copy-source",
                "x-amz-date",
                "x-amz-user-agent",
                "content-md5",
            ],
            "AllowedMethods": ["GET", "PUT", "POST", "DELETE", "HEAD"],
            "AllowedOrigins": [str(settings.ui_uri)[:-1]],
            "ExposeHeaders": [
                "Etag",
            ],
            "MaxAgeSeconds": 120,
        },
    ]
}


def get_anonymously_bucket_policy(bucket_name: str) -> list[dict[str, Any]]:
    return [
        {
            "Sid": ANONYMOUS_ACCESS_SID,
            "Effect": "Allow",
            "Principal": "*",
            "Resource": f"arn:aws:s3:::{bucket_name}/*",
            "Action": ["s3:GetObject"],
        },
        {
            "Sid": ANONYMOUS_ACCESS_SID,
            "Effect": "Allow",
            "Principal": "*",
            "Resource": f"arn:aws:s3:::{bucket_name}",
            "Action": ["s3:ListBucket"],
        },
    ]


@router.get("", response_model=list[BucketOutSchema], summary="List buckets of user")
@start_as_current_span_async("api_list_buckets", tracer=tracer)
async def list_buckets(
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    owner_id: Annotated[
        UUID | SkipJsonSchema[None],
        Query(
            description="UID of the user for whom to fetch the buckets for. Permission 'bucket:read_any' required if current user is not the target.",  # noqa:E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    bucket_type: Annotated[
        CRUDBucket.BucketType, Query(description="Type of the bucket to get. Ignored when `user` parameter not set")
    ] = CRUDBucket.BucketType.ALL,
) -> Iterable[Bucket]:
    """
    List all the buckets in the system or of the desired user where the user has READ permissions for.\n
    Permission `bucket:list` required if the current user is the owner of the bucket,
    otherwise `bucket:list_all` required.
    \f
    Parameters
    ----------
    owner_id : uuid.UUID
        User for which to retrieve the buckets. Dependency Injection.
    bucket_type : app.crud.crud_bucket.CRUDBucket.BucketType, default BucketType.ALL
        Type of the bucket to get. Query Parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    Returns
    -------
    buckets : list[clowmdb.models.Bucket]
        All the buckets for which the user has READ permissions.
    """
    current_span = trace.get_current_span()
    if owner_id is not None:  # pragma: no cover
        current_span.set_attribute("owner_id", str(owner_id))
    current_span.set_attribute("bucket_type", bucket_type.name)
    await authorization("list_all" if current_user.uid != owner_id else "list")
    if owner_id is None:
        buckets = await CRUDBucket.get_all(db=db)
    else:
        buckets = await CRUDBucket.get_for_user(owner_id, bucket_type, db=db)

    return buckets


@router.post(
    "",
    response_model=BucketOutSchema,
    status_code=status.HTTP_201_CREATED,
    summary="Create a bucket for the current user",
)
@start_as_current_span_async("api_create_bucket", tracer=tracer)
async def create_bucket(
    bucket: BucketInSchema,
    current_user: CurrentUser,
    db: DBSession,
    s3: S3Resource,
    authorization: Authorization,
    rgw: RGWAdminResource,
) -> Bucket:
    """
    Create a bucket for the current user.\n
    The name of the bucket has some constraints.
    For more information see the
    [Ceph documentation](https://docs.ceph.com/en/quincy/radosgw/s3/bucketops/#constraints)\n
    Permission `bucket:create` required.
    \f
    Parameters
    ----------
    bucket : app.schemas.bucket.BucketIn
        Information about the bucket to create. HTTP Body
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.

    Returns
    -------
    bucket : clowmdb.models.Bucket
        The newly created bucket.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    await authorization("create")
    try:
        db_bucket = await CRUDBucket.create(
            bucket,
            current_user.uid,
            db=db,
            size_limit=int(settings.s3.default_bucket_size_limit.to("KiB")),
            object_limit=settings.s3.default_bucket_object_limit,
        )
    except DuplicateError as e:
        current_span.record_exception(e)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bucket name is already taken",
        )
    s3_bucket = s3.Bucket(db_bucket.name)
    with tracer.start_as_current_span("s3_create_bucket") as span:
        span.set_attribute("bucket_name", db_bucket.name)
        s3_bucket.create()
    # Add basic permission to the user for getting, creating and deleting objects in the bucket.
    bucket_policy = json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "ProxyOwnerPerm",
                    "Effect": "Allow",
                    "Principal": {"AWS": [f"arn:aws:iam:::user/{settings.s3.username}"]},
                    "Action": ["s3:GetObject"],
                    "Resource": [f"arn:aws:s3:::{db_bucket.name}/*"],
                },
                {
                    "Sid": "PseudoOwnerPerm",
                    "Effect": "Allow",
                    "Principal": {"AWS": [f"arn:aws:iam:::user/{current_user.uid}"]},
                    "Action": [
                        "s3:GetObject",
                        "s3:DeleteObject",
                        "s3:PutObject",
                        "s3:ListBucket",
                        "s3:AbortMultipartUpload",
                        "s3:ListBucketMultipartUploads",
                        "s3:ListMultipartUploadParts",
                    ],
                    "Resource": [f"arn:aws:s3:::{db_bucket.name}/*", f"arn:aws:s3:::{db_bucket.name}"],
                },
            ],
        }
    )
    put_s3_bucket_policy(s3, bucket_name=bucket.name, policy=bucket_policy)
    with tracer.start_as_current_span("s3_put_bucket_cors_rules") as span:
        span.set_attribute("bucket_name", db_bucket.name)
        s3_bucket.Cors().put(CORSConfiguration=cors_rule)  # type: ignore[arg-type]
    rgw_update_bucket_limits(rgw=rgw, bucket=db_bucket)
    return db_bucket


@router.get("/{bucket_name}", response_model=BucketOutSchema, summary="Get a bucket by its name")
@start_as_current_span_async("api_get_bucket", tracer=tracer)
async def get_bucket(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    db: DBSession,
) -> Bucket:
    """
    Get a bucket by its name if the current user has READ permissions for the bucket.\n
    Permission `bucket:read` required if the current user is the owner of the bucket,
    otherwise `bucket:read_any` required.
    \f
    Parameters
    ----------
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.

    Returns
    -------
    bucket : clowmdb.models.Bucket
        Bucket with the provided name.
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    rbac_operation = (
        "read_any"
        if not bucket.public and not await CRUDBucketPermission.check_permission(bucket.name, current_user.uid, db=db)
        else "read"
    )
    await authorization(rbac_operation)
    return bucket


@router.patch("/{bucket_name}/public", response_model=BucketOutSchema, summary="Update public status")
@start_as_current_span_async("api_update_bucket_public_state", tracer=tracer)
async def update_bucket_public_state(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    db: DBSession,
    s3: S3Resource,
    public: Annotated[bool, Body(..., embed=True, description="New State")],
) -> Bucket:
    """
    Update the buckets public state.\n
    Permission `bucket:update` required if the current user is the owner of the bucket,
    otherwise `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    public : bool
        The new public state. HTTP Body.

    Returns
    -------
    bucket : clowmdb.models.Bucket
        Bucket with the toggled public state.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "public": public})
    rbac_operation = "update" if bucket.owner_id == current_user.uid else "update_any"
    await authorization(rbac_operation)
    if bucket.public == public:
        return bucket
    s3_policy = get_s3_bucket_policy(s3, bucket_name=bucket.name)
    json_policy = json.loads(s3_policy.policy)
    if public:
        json_policy["Statement"] += get_anonymously_bucket_policy(bucket.name)

    else:
        json_policy["Statement"] = [stmt for stmt in json_policy["Statement"] if stmt["Sid"] != ANONYMOUS_ACCESS_SID]
    put_s3_bucket_policy(s3, bucket_name=bucket.name, policy=json.dumps(json_policy))
    await CRUDBucket.update_public_state(db=db, public=public, bucket_name=bucket.name)
    return bucket


@router.patch("/{bucket_name}/limits", response_model=BucketOutSchema, summary="Update bucket limits")
@start_as_current_span_async("api_update_bucket_limits", tracer=tracer)
async def update_bucket_limits(
    bucket: CurrentBucket,
    authorization: Authorization,
    db: DBSession,
    rgw: RGWAdminResource,
    limits: BucketSizeLimits,
) -> Bucket:
    """
    Update the buckets size limits.\n
    Permission `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    limits : app.schemas.bucket.BucketSizeLimits
        New bucket limits. HTTP Body.

    Returns
    -------
    bucket : clowmdb.models.Bucket
        Bucket with the toggled public state.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    if limits.size_limit is not None:  # pragma: no cover
        current_span.set_attribute("size_limit", ByteSize(limits.size_limit * 1024).human_readable())
    if limits.object_limit is not None:  # pragma: no cover
        current_span.set_attribute("object_limit", limits.object_limit)
    await authorization("update_any")
    await CRUDBucket.update_bucket_limits(
        db=db, bucket_name=bucket.name, object_limit=limits.object_limit, size_limit=limits.size_limit
    )
    rgw_update_bucket_limits(rgw=rgw, bucket=bucket)
    return bucket


@router.delete("/{bucket_name}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a bucket")
@start_as_current_span_async("api_delete_bucket", tracer=tracer)
async def delete_bucket(
    bucket: CurrentBucket,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Resource,
    force_delete: Annotated[bool, Query(description="Delete even non-empty bucket")] = False,
) -> None:
    """
    Delete a bucket by its name. Only the owner of the bucket can delete the bucket.\n
    Permission `bucket:delete` required if the current user is the owner of the bucket,
    otherwise `bucket:delete_any` required.
    \f
    Parameters
    ----------
    force_delete : bool, default False
        Flag for deleting a non-empty bucket. Query parameter.
    bucket : clowmdb.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "force_delete": force_delete})
    rbac_operation = "delete_any" if bucket.owner_id != current_user.uid else "delete"
    await authorization(rbac_operation)
    if force_delete:
        objs = [{"Key": obj.key} for obj in get_s3_bucket_objects(s3, bucket_name=bucket.name)]
        if len(objs) > 0:
            with tracer.start_as_current_span("s3_delete_objects") as span:
                span.set_attribute("bucket_name", bucket.name)
                s3.Bucket(bucket.name).delete_objects(Delete={"Objects": objs})  # type: ignore
    try:
        with tracer.start_as_current_span("s3_delete_bucket") as span:
            span.set_attribute("bucket_name", bucket.name)
            s3.Bucket(name=bucket.name).delete()
        await CRUDBucket.delete(bucket.name, db=db)
    except ClientError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="Bucket not empty")

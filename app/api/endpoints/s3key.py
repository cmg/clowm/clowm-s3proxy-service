from typing import Annotated, Any, Awaitable, Callable

from fastapi import APIRouter, Depends, HTTPException, Path, status
from opentelemetry import trace
from rgwadmin.exceptions import RGWAdminException

from app.api.dependencies import AuthorizationDependency, CurrentUser, PathUser, RGWAdminResource
from app.ceph.rgw import get_s3_keys
from app.otlp import start_as_current_span_async
from app.schemas.s3key import S3Key

router = APIRouter(prefix="/users/{uid}/keys", tags=["S3Key"])
s3key_authorization = AuthorizationDependency(resource="s3_key")
Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(s3key_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)

AccessID = Annotated[
    str,
    Path(
        ...,
        description="ID of the S3 access key",
        examples=["CRJ6B037V2ZT4U3W17VC"],
    ),
]


@router.get(
    "",
    response_model=list[S3Key],
    summary="Get the S3 Access keys from a user",
)
@start_as_current_span_async("api_list_s3_keys", tracer=tracer)
async def get_user_keys(
    rgw: RGWAdminResource,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> list[S3Key]:
    """
    Get all the S3 Access keys for a specific user.\n
    Permission `s3_key:list` required.
    \f
    Parameters
    ----------
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    user : clowmdb.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    keys : List(app.schemas.user.S3Key)
        All S3 keys from the user.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    if current_user.uid != user.uid:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Action forbidden.")
    await authorization("list")
    return get_s3_keys(rgw, user.uid)


@router.post(
    "",
    response_model=S3Key,
    summary="Create a Access key for a user",
    status_code=status.HTTP_201_CREATED,
)
@start_as_current_span_async("api_create_s3_key", tracer=tracer)
async def create_user_key(
    rgw: RGWAdminResource,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> S3Key:
    """
    Create a S3 Access key for a specific user.\n
    Permission `s3_key:create` required.
    \f
    Parameters
    ----------
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    user : clowmdb.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    key : app.schemas.s3key.S3Key
        Newly created S3 key.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    if current_user.uid != user.uid:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Action forbidden.")
    await authorization("create")
    with tracer.start_as_current_span("rgw_list_keys", attributes={"uid": str(user.uid)}):
        before_keys_set = set(map(lambda key: key.access_key, get_s3_keys(rgw, user.uid)))
    with tracer.start_as_current_span("rgw_create_key", attributes={"uid": str(user.uid)}):
        # create keys returns all keys for a user including the new one
        after_keys = rgw.create_key(uid=user.uid, key_type="s3", generate_key=True)
    new_key_id = list(set(map(lambda key: key["access_key"], after_keys)) - before_keys_set)[0]  # find ID of the key
    index = [key["access_key"] for key in after_keys].index(new_key_id)  # find new key by ID
    return S3Key(uid=user.uid, **after_keys[index])


@router.get(
    "/{access_id}",
    response_model=S3Key,
    summary="Get a specific S3 Access key from a user",
)
@start_as_current_span_async("api_get_s3_key", tracer=tracer)
async def get_user_key(
    rgw: RGWAdminResource,
    current_user: CurrentUser,
    authorization: Authorization,
    access_id: AccessID,
    user: PathUser,
) -> S3Key:
    """
    Get a specific S3 Access Key for a specific user.\n
    Permission `s3_key:read` required.
    \f
    Parameters
    ----------
    access_id : str
        ID of the requested S3 key. URL Path Parameter.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    user : clowmdb.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    key : app.schemas.s3key.S3Key
        Requested S3 key.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    if current_user.uid != user.uid:
        raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Action forbidden.")
    await authorization("read")
    keys = get_s3_keys(rgw, user.uid)
    try:
        index = [key.access_key for key in keys].index(access_id)
        return keys[index]
    except ValueError:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Key not found")


@router.delete(
    "/{access_id}",
    summary="Delete a specific S3 Access key from a user",
    status_code=status.HTTP_204_NO_CONTENT,
)
@start_as_current_span_async("api_delete_s3_key", tracer=tracer)
async def delete_user_key(
    access_id: AccessID,
    rgw: RGWAdminResource,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> None:
    """
    Delete a specific S3 Access key for a specific user.\n
    Permission `s3_key:delete` required if the current user is the target, otherwise `s3_key:delete_any` required.
    \f
    Parameters
    ----------
    access_id : str
        ID of the S3 key to delete. URL Path Parameter.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    user : clowmdb.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    await authorization("delete" if current_user.uid == user.uid else "delete_any")
    if len(get_s3_keys(rgw, user.uid)) <= 1:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="It's not possible to delete the last key")
    try:
        with tracer.start_as_current_span("rgw_delete_key", attributes={"uid": str(user.uid)}):
            rgw.remove_key(access_key=access_id, uid=str(user.uid))
    except RGWAdminException:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Key not found")

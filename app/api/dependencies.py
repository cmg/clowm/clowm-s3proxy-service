from typing import TYPE_CHECKING, Annotated, AsyncGenerator, Awaitable, Callable
from uuid import UUID

from authlib.jose.errors import BadSignatureError, DecodeError, ExpiredTokenError
from clowmdb.db.session import get_async_session
from clowmdb.models import Bucket, User
from fastapi import Depends, HTTPException, Path, Request, status
from fastapi.security import HTTPBearer
from fastapi.security.http import HTTPAuthorizationCredentials
from httpx import AsyncClient
from opentelemetry import trace
from rgwadmin import RGWAdmin
from sqlalchemy.ext.asyncio import AsyncSession

from app.ceph.rgw import rgw
from app.ceph.s3 import s3_resource
from app.core.config import settings
from app.core.security import decode_token, request_authorization
from app.crud.crud_bucket import CRUDBucket
from app.crud.crud_user import CRUDUser
from app.otlp import start_as_current_span_async
from app.schemas.security import JWT, AuthzRequest, AuthzResponse

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

tracer = trace.get_tracer_provider().get_tracer(__name__)
bearer_token = HTTPBearer(description="JWT Header", auto_error=False)


def get_rgw_admin() -> RGWAdmin:  # pragma: no cover
    return rgw


RGWAdminResource = Annotated[RGWAdmin, Depends(get_rgw_admin)]


def get_s3_resource() -> S3ServiceResource:
    return s3_resource  # pragma: no cover


S3Resource = Annotated[S3ServiceResource, Depends(get_s3_resource)]


async def get_db() -> AsyncGenerator[AsyncSession, None]:  # pragma: no cover
    """
    Get a Session with the database.

    FastAPI Dependency Injection Function.

    Returns
    -------
    db : AsyncGenerator[AsyncSession, None]
        Async session object with the database
    """
    async with get_async_session(str(settings.db.dsn_async), verbose=settings.db.verbose) as db:
        yield db


DBSession = Annotated[AsyncSession, Depends(get_db)]


async def get_httpx_client(request: Request) -> AsyncClient:  # pragma: no cover
    # Fetch open http client from the app
    return request.app.requests_client


HTTPXClient = Annotated[AsyncClient, Depends(get_httpx_client)]


def get_decode_jwt_function() -> Callable[[str], dict[str, str]]:  # pragma: no cover
    """
    Get function to decode and verify the JWT.

    This will be injected into the function which will handle the JWT. With this approach, the function to decode and
    verify the JWT can be overwritten during tests.

    Returns
    -------
    decode : Callable[[str], dict[str, str]]
        Function to decode & verify the token. raw_token -> claims. Dependency Injection
    """
    return decode_token


@start_as_current_span_async("decode_jwt", tracer=tracer)
async def decode_bearer_token(
    token: Annotated[HTTPAuthorizationCredentials | None, Depends(bearer_token)],
    decode: Annotated[Callable[[str], dict[str, str]], Depends(get_decode_jwt_function)],
    db: DBSession,
) -> JWT:
    """
    Get the decoded JWT or reject request if it is not valid.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : fastapi.security.http.HTTPAuthorizationCredentials
        Bearer token sent with the HTTP request. Dependency Injection.
    decode : Callable[[str], dict[str, str]]
        Function to decode & verify the token. raw_token -> claims. Dependency Injection
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    """
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    try:
        jwt = JWT(**decode(token.credentials), raw_token=token.credentials)
        trace.get_current_span().set_attributes({"exp": jwt.exp.isoformat(), "uid": jwt.sub})
        await get_current_user(jwt, db)  # make sure the user exists
        return jwt
    except ExpiredTokenError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="JWT Signature has expired")
    except (DecodeError, BadSignatureError):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")


async def get_current_user(token: Annotated[JWT, Depends(decode_bearer_token)], db: DBSession) -> User:
    """
    Get the current user from the database based on the JWT.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : clowmdb.models.User
        User associated with the JWT sent with the HTTP request.
    """
    try:
        uid = UUID(token.sub)
    except ValueError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")
    user = await CRUDUser.get(uid, db=db)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


CurrentUser = Annotated[User, Depends(get_current_user)]


class AuthorizationDependency:
    """
    Class to parameterize the authorization request with the resource to perform an operation on.
    """

    def __init__(self, resource: str):
        """
        Parameters
        ----------
        resource : str
            Resource parameter for the authorization requests
        """
        self.resource = resource

    def __call__(
        self,
        user: CurrentUser,
        client: HTTPXClient,
    ) -> Callable[[str], Awaitable[AuthzResponse]]:
        """
        Get the function to request the authorization service with the resource, JWT and HTTP Client already injected.

        Parameters
        ----------
        user : clowmdb.models.User
            The current user based on the JWT. Dependency Injection.
        client : httpx.AsyncClient
            HTTP Client with an open connection. Dependency Injection.

        Returns
        -------
        authorization_function : Callable[[str], Awaitable[app.schemas.security.AuthzResponse]]
            Async function which ask the Auth service for authorization. It takes the operation as the only input.
        """

        async def authorization_wrapper(operation: str) -> AuthzResponse:
            params = AuthzRequest(operation=operation, resource=self.resource, uid=user.lifescience_id)
            return await request_authorization(request_params=params, client=client)

        return authorization_wrapper


async def get_user_by_path_uid(
    uid: UUID = Path(default=..., description="UID of a user", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]),
    db: AsyncSession = Depends(get_db),
) -> User:
    """
    Get the user from the database with the given uid.
    Reject the request if the current user is not the same as the requested one.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    uid : str
        The uid of a user. URL path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : clowmdb.models.User
        User with the given uid.

    """
    user = await CRUDUser.get(uid, db=db)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


PathUser = Annotated[User, Depends(get_user_by_path_uid)]


async def get_current_bucket(
    bucket_name: Annotated[
        str, Path(..., description="Name of bucket", examples=["test-bucket"], max_length=63, min_length=3)
    ],
    db: DBSession,
) -> Bucket:
    """
    Get the Bucket from the database based on the name in the path.
    Reject the request if user has no permission for this bucket.

    FastAPI Dependency Injection Function

    Parameters
    ----------
    bucket_name : str
        Name of a bucket. URL Path Parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    bucket : clowmdb.models.Bucket
        Bucket with the given name.
    """
    bucket = await CRUDBucket.get(bucket_name, db=db)
    if bucket is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Bucket not found")
    return bucket


CurrentBucket = Annotated[Bucket, Depends(get_current_bucket)]

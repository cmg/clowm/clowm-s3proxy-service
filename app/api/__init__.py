from typing import Any

from fastapi import APIRouter, Depends, status

from app.schemas.security import ErrorDetail

from .dependencies import decode_bearer_token
from .endpoints import bucket_permission_router, bucket_router, miscellaneous_router, s3key_router

__all__ = ["api_router"]

alternative_responses: dict[int | str, dict[str, Any]] = {
    status.HTTP_400_BAD_REQUEST: {
        "model": ErrorDetail,
        "description": "Error decoding JWT Token",
        "content": {"application/json": {"example": {"detail": "Malformed JWT Token"}}},
    },
    status.HTTP_401_UNAUTHORIZED: {
        "model": ErrorDetail,
        "description": "Not authenticated",
        "content": {"application/json": {"example": {"detail": "Not authenticated"}}},
    },
    status.HTTP_403_FORBIDDEN: {
        "model": ErrorDetail,
        "description": "Not authorized",
        "content": {"application/json": {"example": {"detail": "Not authorized"}}},
    },
    status.HTTP_404_NOT_FOUND: {
        "model": ErrorDetail,
        "description": "Entity not Found",
        "content": {"application/json": {"example": {"detail": "Entity not found."}}},
    },
}

api_router = APIRouter()
api_router.include_router(
    bucket_router,
    dependencies=[Depends(decode_bearer_token)],
    responses=alternative_responses,
)
api_router.include_router(
    s3key_router,
    dependencies=[Depends(decode_bearer_token)],
    responses=alternative_responses,
)
api_router.include_router(
    bucket_permission_router,
    dependencies=[Depends(decode_bearer_token)],
    responses=alternative_responses,
)
api_router.include_router(miscellaneous_router)

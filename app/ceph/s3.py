from typing import TYPE_CHECKING

from boto3 import resource
from opentelemetry import trace

from app.core.config import settings

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import BucketPolicy, ObjectSummary, S3ServiceResource
else:
    S3ServiceResource = object
    BucketPolicy = object
    ObjectSummary = object


tracer = trace.get_tracer_provider().get_tracer(__name__)

s3_resource: S3ServiceResource = resource(
    service_name="s3",
    endpoint_url=str(settings.s3.uri)[:-1],
    aws_access_key_id=settings.s3.access_key,
    aws_secret_access_key=settings.s3.secret_key.get_secret_value(),
    verify=settings.s3.uri.scheme == "https",
)


def get_s3_bucket_policy(s3: S3ServiceResource, bucket_name: str) -> BucketPolicy:
    with tracer.start_as_current_span("s3_get_bucket_policy") as span:
        span.set_attribute("bucket_name", bucket_name)
        s3_policy = s3.Bucket(bucket_name).Policy()
        s3_policy.load()
        return s3_policy


def put_s3_bucket_policy(s3: S3ServiceResource, bucket_name: str, policy: str) -> None:
    with tracer.start_as_current_span("s3_put_bucket_policy") as span:
        span.set_attribute("bucket_name", bucket_name)
        s3.Bucket(bucket_name).Policy().put(Policy=policy)


def get_s3_bucket_objects(s3: S3ServiceResource, bucket_name: str) -> list[ObjectSummary]:
    with tracer.start_as_current_span("s3_get_object_meta_data") as span:
        span.set_attribute("bucket_name", bucket_name)
        return list(s3.Bucket(bucket_name).objects.all())

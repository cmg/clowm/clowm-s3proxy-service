from enum import StrEnum, unique
from typing import Sequence
from uuid import UUID

from clowmdb.models import Bucket
from clowmdb.models import BucketPermission as BucketPermissionDB
from opentelemetry import trace
from pydantic import ByteSize
from sqlalchemy import delete, func, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud.crud_error import DuplicateError
from app.schemas.bucket import BucketIn as BucketInSchema

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDBucket:
    @unique
    class BucketType(StrEnum):
        """
        Enumeration for the type of buckets to fetch from the DB

        OWN: Only fetch buckets that the user owns
        PERMISSION: Only fetch foreign buckets that the user has access to
        ALL: Fetch all buckets that the user has access to
        """

        OWN = "OWN"
        ALL = "ALL"
        PERMISSION = "PERMISSION"

    @staticmethod
    async def get(bucket_name: str, *, db: AsyncSession) -> Bucket | None:
        """
        Get a bucket by its name.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        bucket_name : str
            Name of the Bucket to get from database.

        Returns
        -------
        bucket : Bucket | None
            Returns the bucket if it exists, None otherwise.
        """
        stmt = select(Bucket).where(Bucket.name == bucket_name)
        with tracer.start_as_current_span(
            "db_get_bucket", attributes={"bucket_name": bucket_name, "sql_query": str(stmt)}
        ):
            return await db.scalar(stmt)

    @staticmethod
    async def get_all(db: AsyncSession) -> Sequence[Bucket]:
        stmt = select(Bucket)
        with tracer.start_as_current_span("db_list_all_buckets", attributes={"sql_query": str(stmt)}):
            return (await db.scalars(stmt)).all()

    @staticmethod
    async def get_for_user(
        uid: UUID, bucket_type: BucketType = BucketType.ALL, *, db: AsyncSession
    ) -> Sequence[Bucket]:
        """
        Get all buckets for a user. Depending on the `bucket_type`, the user is either owner of the bucket or has
        permission for the bucket

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        uid : str
            UID of a user.
        bucket_type : BucketType, default BucketType.ALL


        Returns
        -------
        buckets : list[clowmdb.models.Bucket]
            A list of all buckets where the given user has READ permissions for.

        Notes
        -----
        SQL Query own buckets:
        ```
        SELECT bucket.name, bucket.description, bucket.public, bucket.owner_id
        FROM bucket
        WHERE bucket.owner_id = %s
        ```

        SQL Query all buckets that the user has access to:
        ```
        SELECT bucket.name, bucket.description, bucket.public, bucket.owner_id
        FROM bucket
        WHERE bucket.owner_id = %s OR (EXISTS
            (SELECT 1 FROM bucketpermission
            WHERE bucket.name = bucketpermission.bucket_name AND bucketpermission.user_id = %s
                AND(UNIX_TIMESTAMP() >= bucketpermission.`from` 0 OR bucketpermission.`from` IS NULL)
                AND(UNIX_TIMESTAMP() <= bucketpermission.`to` >= 0 OR bucketpermission.`to` IS NULL)))
        ```

        SQL Query only foreign buckets where user has permission to
        ```
        SELECT bucket.name, bucket.description, bucket.public, bucket.owner_id
        FROM bucket
        WHERE (EXISTS
            (SELECT 1 FROM bucketpermission
            WHERE bucket.name = bucketpermission.bucket_name AND bucketpermission.user_id = %s
                AND(UNIX_TIMESTAMP() >=  bucketpermission.`from` <= 0 OR bucketpermission.`from` IS NULL)
                AND(UNIX_TIMESTAMP() <=  bucketpermission.`to` >= 0 OR bucketpermission.`to` IS NULL)))
        ```
        """
        stmt = select(Bucket)
        if bucket_type == CRUDBucket.BucketType.ALL:
            stmt = stmt.where(
                or_(
                    Bucket.owner_id_bytes == uid.bytes,
                    Bucket.permissions.any(BucketPermissionDB.uid_bytes == uid.bytes)
                    .where(
                        or_(
                            func.UNIX_TIMESTAMP() >= BucketPermissionDB.from_,
                            BucketPermissionDB.from_ == None,  # noqa:E711
                        )
                    )
                    .where(
                        or_(
                            func.UNIX_TIMESTAMP() <= BucketPermissionDB.to,
                            BucketPermissionDB.to == None,  # noqa:E711
                        )
                    ),
                )
            )
        elif bucket_type == CRUDBucket.BucketType.OWN:
            stmt = stmt.where(Bucket.owner_id_bytes == uid.bytes)
        else:
            stmt = stmt.where(
                Bucket.permissions.any(BucketPermissionDB.uid_bytes == uid.bytes)
                .where(
                    or_(
                        func.UNIX_TIMESTAMP() >= BucketPermissionDB.from_,
                        BucketPermissionDB.from_ == None,  # noqa:E711
                    )
                )
                .where(
                    or_(
                        func.UNIX_TIMESTAMP() <= BucketPermissionDB.to,
                        BucketPermissionDB.to == None,  # noqa:E711
                    )
                ),
            )
        with tracer.start_as_current_span(
            "db_list_buckets_for_user",
            attributes={"sql_query": str(stmt), "uid": str(uid), "bucket_type": bucket_type.name},
        ):
            return (await db.scalars(stmt)).all()

    @staticmethod
    async def create(
        bucket_in: BucketInSchema,
        uid: UUID,
        size_limit: int | None = None,
        object_limit: int | None = None,
        *,
        db: AsyncSession,
    ) -> Bucket:
        """
        Create a bucket for a given user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        bucket_in : app.schemas.bucket.BucketIn
            All relevant information for a new bucket.
        uid : str
            UID of the owner for the new bucket.

        Returns
        -------
        bucket : clowmdb.models.Bucket
            Returns the created bucket.
        """
        bucket = Bucket(
            **bucket_in.model_dump(), owner_id_bytes=uid.bytes, size_limit=size_limit, object_limit=object_limit
        )
        with tracer.start_as_current_span(
            "db_create_bucket",
            attributes={"uid": str(uid), "bucket_name": bucket.name},
        ):
            if await CRUDBucket.get(bucket.name, db=db) is not None:
                raise DuplicateError(f"Bucket {bucket.name} exists already")
            db.add(bucket)
            await db.commit()
            return bucket

    @staticmethod
    async def update_public_state(bucket_name: str, public: bool, *, db: AsyncSession) -> None:
        """
        Update the public state of a bucket

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        bucket_name : str
            Name of a bucket.
        public : bool
            New public state of the bucket.
        """
        stmt = update(Bucket).where(Bucket.name == bucket_name).values(public=public)
        with tracer.start_as_current_span(
            "db_update_bucket_public_state",
            attributes={"bucket_name": bucket_name, "public": public, "sql_query": str(stmt)},
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def update_bucket_limits(
        bucket_name: str, size_limit: int | None = None, object_limit: int | None = None, *, db: AsyncSession
    ) -> None:
        """
        Update the bucket limits.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        bucket_name : str
            Name of a bucket.
        size_limit : int | None, default None
            New size limit for the bucket.
        object_limit : int | None, default None
            New object limit for the bucket.
        """
        stmt = update(Bucket).where(Bucket.name == bucket_name).values(size_limit=size_limit, object_limit=object_limit)
        with tracer.start_as_current_span(
            "db_update_bucket_limits", attributes={"bucket_name": bucket_name, "sql_query": str(stmt)}
        ) as span:
            if size_limit is not None:  # pragma: no cover
                span.set_attribute("size_limit", ByteSize(size_limit * 1024).human_readable())
            if object_limit is not None:  # pragma: no cover
                span.set_attribute("object_limit", object_limit)
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def delete(bucket_name: str, *, db: AsyncSession) -> None:
        """
        Delete a specific bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        bucket_name : string
            The name of the bucket to delete.
        """
        stmt = delete(Bucket).where(Bucket.name == bucket_name)
        with tracer.start_as_current_span(
            "db_delete_bucket",
            attributes={"sql_query": str(stmt), "bucket_name": bucket_name},
        ):
            await db.execute(stmt)
            await db.commit()

from uuid import UUID

from clowmdb.models import User
from opentelemetry import trace
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDUser:
    @staticmethod
    async def get(uid: UUID, *, db: AsyncSession) -> User | None:
        """
        Get a user by its UID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        uid : str
            UID of a user.

        Returns
        -------
        user : clowmdb.models.User | None
            The user for the given UID if he exists, None otherwise
        """
        stmt = select(User).where(User.uid_bytes == uid.bytes)
        with tracer.start_as_current_span("db_get_user", attributes={"uid": str(uid), "sql_query": str(stmt)}):
            return await db.scalar(stmt)

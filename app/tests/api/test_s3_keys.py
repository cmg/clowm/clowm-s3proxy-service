import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter

from app.schemas.s3key import S3Key
from app.tests.mocks.mock_rgw_admin import MockRGWAdmin
from app.tests.utils.user import UserWithAuthHeader


class _TestS3KeyRoutes:
    base_path = "/users"


class TestS3KeyRoutesGet(_TestS3KeyRoutes):
    @pytest.mark.asyncio
    async def test_get_s3_keys_for_foreign_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_second_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting the S3 keys from a foreign user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random foreign user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_second_user.user.uid)}/keys", headers=random_user.auth_headers
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_get_s3_keys_for_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting the S3 keys from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_user.user.uid)}/keys", headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[S3Key])
        keys = ta.validate_json(response.content)
        assert len(keys) == 1
        assert keys[0].uid == random_user.user.uid

    @pytest.mark.asyncio
    async def test_get_specific_s3_key_for_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_rgw_admin: MockRGWAdmin,
    ) -> None:
        """
        Test for getting a specific S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock class for rgwadmin package.
        """
        s3_key = mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"][0]
        response = await client.get(
            f"{self.base_path}/{str(random_user.user.uid)}/keys/{s3_key['access_key']}",
            headers=random_user.auth_headers,
        )
        response_key = S3Key.model_validate_json(response.content)
        assert response.status_code == status.HTTP_200_OK
        assert response_key.access_key == s3_key["access_key"]
        assert response_key.secret_key == s3_key["secret_key"]
        assert str(response_key.uid) == s3_key["user"]

    @pytest.mark.asyncio
    async def test_get_specific_s3_key_from_foreign_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_second_user: UserWithAuthHeader,
        mock_rgw_admin: MockRGWAdmin,
    ) -> None:
        """
        Test for getting a specific S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock class for rgwadmin package.
        """
        s3_key = mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"][0]
        response = await client.get(
            f"{self.base_path}/{str(random_user.user.uid)}/keys/{s3_key['access_key']}",
            headers=random_second_user.auth_headers,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_get_unknown_s3_key_for_user(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting an unknown S3 keys from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_user.user.uid)}/keys/impossible_key", headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestS3KeyRoutesCreate(_TestS3KeyRoutes):
    @pytest.mark.asyncio
    async def test_create_s3_key_for_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_rgw_admin: MockRGWAdmin,
    ) -> None:
        """
        Test for getting a specific S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock class for rgwadmin package.
        """
        old_s3_key = mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"][0]
        response = await client.post(
            f"{self.base_path}/{str(random_user.user.uid)}/keys", headers=random_user.auth_headers
        )
        new_key = S3Key.model_validate_json(response.content)

        assert response.status_code == status.HTTP_201_CREATED
        assert new_key.access_key != old_s3_key["access_key"]
        assert new_key.uid == random_user.user.uid

    @pytest.mark.asyncio
    async def test_create_s3_key_for_foreign_user(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a specific S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        response = await client.post(
            f"{self.base_path}/{str(random_second_user.user.uid)}/keys", headers=random_user.auth_headers
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestS3KeyRoutesDelete(_TestS3KeyRoutes):
    @pytest.mark.asyncio
    async def test_delete_s3_key_for_user(
        self, client: AsyncClient, random_user: UserWithAuthHeader, mock_rgw_admin: MockRGWAdmin
    ) -> None:
        """
        Test for deleting a specific S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock class for rgwadmin package.
        """
        new_s3_key = mock_rgw_admin.create_key(uid=str(random_user.user.uid))[-1]
        assert len(mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"]) == 2
        response = await client.delete(
            f"{self.base_path}/{str(random_user.user.uid)}/keys/{new_s3_key['access_key']}",
            headers=random_user.auth_headers,
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert len(mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"]) == 1

    @pytest.mark.asyncio
    async def test_delete_last_s3_key_for_user(
        self, client: AsyncClient, random_user: UserWithAuthHeader, mock_rgw_admin: MockRGWAdmin
    ) -> None:
        """
        Test for deleting the last S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock class for rgwadmin package.
        """
        assert len(mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"]) == 1
        key_id = mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"][0]
        response = await client.delete(
            f"{self.base_path}/{str(random_user.user.uid)}/keys/{key_id}", headers=random_user.auth_headers
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert len(mock_rgw_admin.get_user(uid=str(random_user.user.uid))["keys"]) == 1

    @pytest.mark.asyncio
    async def test_delete_unknown_s3_key_for_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_rgw_admin: MockRGWAdmin,
    ) -> None:
        """
        Test for deleting an unknown S3 key from a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_rgw_admin : app.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock class for rgwadmin package.
        """
        mock_rgw_admin.create_key(uid=str(random_user.user.uid))
        response = await client.delete(
            f"{self.base_path}/{str(random_user.user.uid)}/keys/impossible", headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

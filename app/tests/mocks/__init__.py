from abc import ABC, abstractmethod

from fastapi import status
from httpx import Request, Response


class MockHTTPService(ABC):
    def __init__(self) -> None:
        self.send_error = False

    @abstractmethod
    def handle_request(self, request: Request) -> Response: ...

    def reset(self) -> None:
        self.send_error = False


class DefaultMockHTTPService(MockHTTPService):
    def handle_request(self, request: Request) -> Response:
        return Response(
            status_code=status.HTTP_404_NOT_FOUND if self.send_error else status.HTTP_200_OK,
            json={},
        )

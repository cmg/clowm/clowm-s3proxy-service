import asyncio
import json
from functools import partial
from secrets import token_urlsafe
from typing import AsyncIterator, Generator, Iterator

import httpx
import pytest
import pytest_asyncio
from clowmdb.db.session import get_async_session
from clowmdb.models import Bucket
from clowmdb.models import BucketPermission as BucketPermissionDB
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.dependencies import get_db, get_decode_jwt_function, get_httpx_client, get_rgw_admin, get_s3_resource
from app.core.config import settings
from app.main import app
from app.schemas.bucket_permission import BucketPermissionOut as BucketPermissionSchema
from app.tests.mocks import DefaultMockHTTPService, MockHTTPService
from app.tests.mocks.mock_opa_service import MockOpaService
from app.tests.mocks.mock_rgw_admin import MockRGWAdmin
from app.tests.mocks.mock_s3_resource import MockS3ServiceResource
from app.tests.utils.bucket import create_random_bucket
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader, create_random_user, decode_mock_token, get_authorization_headers

jwt_secret = token_urlsafe(32)


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def mock_rgw_admin() -> MockRGWAdmin:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockRGWAdmin()


@pytest.fixture(scope="session")
def mock_s3_service() -> MockS3ServiceResource:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockS3ServiceResource()


@pytest.fixture(scope="session")
def mock_opa_service() -> Iterator[MockOpaService]:
    mock_opa = MockOpaService()
    yield mock_opa
    mock_opa.reset()


@pytest_asyncio.fixture(scope="session")
async def mock_client(
    mock_opa_service: MockOpaService,
) -> AsyncIterator[httpx.AsyncClient]:
    def mock_request_handler(request: httpx.Request) -> httpx.Response:
        url = str(request.url)
        handler: MockHTTPService
        if url.startswith(str(settings.opa.uri)):
            handler = mock_opa_service
        else:
            handler = DefaultMockHTTPService()
        return handler.handle_request(request)

    async with httpx.AsyncClient(transport=httpx.MockTransport(mock_request_handler)) as http_client:
        yield http_client


@pytest_asyncio.fixture(scope="module")
async def client(
    mock_s3_service: MockS3ServiceResource,
    db: AsyncSession,
    mock_opa_service: MockOpaService,
    mock_client: httpx.AsyncClient,
    mock_rgw_admin: MockRGWAdmin,
) -> AsyncIterator[httpx.AsyncClient]:
    """
    Fixture for creating a TestClient and perform HTTP Request on it.
    Overrides several dependencies.
    """

    app.dependency_overrides[get_rgw_admin] = lambda: mock_rgw_admin
    app.dependency_overrides[get_httpx_client] = lambda: mock_client
    app.dependency_overrides[get_s3_resource] = lambda: mock_s3_service
    app.dependency_overrides[get_decode_jwt_function] = lambda: partial(decode_mock_token, secret=jwt_secret)
    app.dependency_overrides[get_db] = lambda: db
    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app), base_url="http://localhost") as ac:  # type: ignore[arg-type]
        yield ac
    app.dependency_overrides = {}


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncIterator[AsyncSession]:
    """
    Fixture for creating a database session to connect to.
    """
    async with get_async_session(url=str(settings.db.dsn_async), verbose=settings.db.verbose) as dbSession:
        yield dbSession


@pytest_asyncio.fixture(scope="function")
async def random_user(
    db: AsyncSession, mock_rgw_admin: MockRGWAdmin, mock_opa_service: MockOpaService
) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    mock_opa_service.add_user(user.lifescience_id, privileged=True)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_opa_service.delete_user(user.lifescience_id)
    mock_rgw_admin.delete_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_second_user(
    db: AsyncSession, mock_rgw_admin: MockRGWAdmin, mock_opa_service: MockOpaService
) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random second user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    mock_opa_service.add_user(user.lifescience_id)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_opa_service.delete_user(user.lifescience_id)
    mock_rgw_admin.delete_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_third_user(
    db: AsyncSession, mock_rgw_admin: MockRGWAdmin, mock_opa_service: MockOpaService
) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random third user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    mock_opa_service.add_user(user.lifescience_id)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_opa_service.delete_user(user.lifescience_id)
    mock_rgw_admin.delete_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket(
    db: AsyncSession, random_user: UserWithAuthHeader, mock_s3_service: MockS3ServiceResource
) -> AsyncIterator[Bucket]:
    """
    Create a random bucket and deletes it afterwards.
    """
    bucket = await create_random_bucket(db, random_user.user)
    mock_s3_service.Bucket(name=bucket.name).create()
    mock_s3_service.BucketPolicy(bucket.name).put(
        json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "PseudoOwnerPerm",
                        "Effect": "Allow",
                        "Principal": {"AWS": [f"arn:aws:iam:::user/{str(random_user.user.uid)}"]},
                        "Action": ["s3:GetObject", "s3:DeleteObject", "s3:PutObject", "s3:ListBucket"],
                        "Resource": [f"arn:aws:s3:::{bucket.name}/*", f"arn:aws:s3:::{bucket.name}"],
                    }
                ],
            }
        )
    )
    yield bucket
    mock_s3_service.delete_bucket(name=bucket.name, force_delete=True)
    await db.delete(bucket)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket_permission(
    db: AsyncSession,
    random_second_user: UserWithAuthHeader,
    random_bucket: Bucket,
    mock_s3_service: MockS3ServiceResource,
) -> BucketPermissionDB:
    """
    Create a bucket READ permission for the second user on a bucket.
    """
    permission_db = BucketPermissionDB(uid_bytes=random_second_user.user.uid.bytes, bucket_name=random_bucket.name)
    db.add(permission_db)
    await db.commit()
    await db.refresh(permission_db)
    mock_s3_service.Bucket(random_bucket.name).Policy().put(
        json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": BucketPermissionSchema.from_db_model(permission_db).map_to_bucket_policy_statement(
                    random_second_user.user.uid
                ),
            }
        )
    )
    return permission_db


@pytest_asyncio.fixture(scope="function")
async def random_bucket_permission_schema(
    random_bucket_permission: BucketPermissionDB, random_second_user: UserWithAuthHeader
) -> BucketPermissionSchema:
    """
    Create a bucket READ permission for the second user on a bucket.
    """

    return BucketPermissionSchema.from_db_model(random_bucket_permission)


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncIterator[CleanupList]:
    """
    Yields a Cleanup object where (async) functions can be registered which get executed after a (failed) test
    """
    cleanup_list = CleanupList()
    yield cleanup_list
    await cleanup_list.empty_queue()

from datetime import datetime, timedelta
from uuid import uuid4

import pytest
from clowmdb.models import Bucket
from clowmdb.models import BucketPermission as BucketPermissionDB
from sqlalchemy import and_, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDBucketPermission, DuplicateError
from app.schemas.bucket_permission import BucketPermissionIn as BucketPermissionSchema
from app.schemas.bucket_permission import BucketPermissionParameters as BucketPermissionParametersSchema
from app.tests.utils.bucket import add_permission_for_bucket
from app.tests.utils.user import UserWithAuthHeader


class TestBucketPermissionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_specific_bucket_permission(
        self, db: AsyncSession, random_bucket_permission: BucketPermissionDB
    ) -> None:
        """
        Test for getting a specific bucket permission from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowmdb.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        bucket_permission = await CRUDBucketPermission.get(
            bucket_name=random_bucket_permission.bucket_name, uid=random_bucket_permission.uid, db=db
        )
        assert bucket_permission
        assert bucket_permission.uid == random_bucket_permission.uid
        assert bucket_permission.bucket_name == random_bucket_permission.bucket_name
        assert bucket_permission.permissions == random_bucket_permission.permissions

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_by_bucket_name(
        self, db: AsyncSession, random_bucket_permission: BucketPermissionDB
    ) -> None:
        """
        Test for getting all bucket permissions for a specific bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowmdb.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        bucket_permissions = await CRUDBucketPermission.list(db=db, bucket_name=random_bucket_permission.bucket_name)
        assert len(bucket_permissions) == 1
        bucket_permission = bucket_permissions[0]
        assert bucket_permission.uid == random_bucket_permission.uid
        assert bucket_permission.bucket_name == random_bucket_permission.bucket_name
        assert bucket_permission.permissions == random_bucket_permission.permissions

    @pytest.mark.asyncio
    async def test_get_read_bucket_permissions_by_bucket_name(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting only 'READ' bucket permissions for a specific bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(db, random_bucket.name, random_second_user.user.uid)
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.user.uid, permission=BucketPermissionDB.Permission.WRITE
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_types=[BucketPermissionDB.Permission.READ]
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.user.uid

    @pytest.mark.asyncio
    async def test_get_read_and_write_bucket_permissions_by_bucket_name(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all 'READ' and 'WRITE' bucket permissions for a specific bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(db, random_bucket.name, random_second_user.user.uid)
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.user.uid, permission=BucketPermissionDB.Permission.WRITE
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db,
            bucket_name=random_bucket.name,
            permission_types=[BucketPermissionDB.Permission.READ, BucketPermissionDB.Permission.WRITE],
        )
        assert len(bucket_permissions) == 2
        assert random_second_user.user.uid in map(lambda x: x.uid, bucket_permissions)
        assert random_third_user.user.uid in map(lambda x: x.uid, bucket_permissions)

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_bucket_name1(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'from' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, from_=datetime.now() + timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.user.uid, from_=datetime.now() - timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_status=CRUDBucketPermission.PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_third_user.user.uid

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_bucket_name2(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'to' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, to=datetime.now() - timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.user.uid, to=datetime.now() + timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_status=CRUDBucketPermission.PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_third_user.user.uid

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_bucket_name3(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        The 'from' and 'to' timestamp are set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.user.uid,
            to=datetime.now() - timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=2),
        )
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_third_user.user.uid,
            to=datetime.now() + timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=1),
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_status=CRUDBucketPermission.PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_third_user.user.uid

    @pytest.mark.asyncio
    async def test_get_inactive_bucket_permissions_by_bucket_name1(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all inactive bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'from' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, from_=datetime.now() + timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.user.uid, from_=datetime.now() - timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_status=CRUDBucketPermission.PermissionStatus.INACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.user.uid

    @pytest.mark.asyncio
    async def test_get_inactive_bucket_permissions_by_bucket_name2(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all inactive bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'to' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, to=datetime.now() - timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.user.uid, to=datetime.now() + timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_status=CRUDBucketPermission.PermissionStatus.INACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.user.uid

    @pytest.mark.asyncio
    async def test_get_inactive_bucket_permissions_by_bucket_name3(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        random_third_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all inactive bucket permissions for a specific bucket from the CRUD Repository.
        The 'from' and 'to' timestamp are set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        random_third_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.user.uid,
            to=datetime.now() - timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=2),
        )
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_third_user.user.uid,
            to=datetime.now() + timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=1),
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, bucket_name=random_bucket.name, permission_status=CRUDBucketPermission.PermissionStatus.INACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.user.uid

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_by_uid(
        self, db: AsyncSession, random_bucket_permission: BucketPermissionDB
    ) -> None:
        """
        Test for getting all bucket permissions for a specific user from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowmdb.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        bucket_permissions = await CRUDBucketPermission.list(db=db, uid=random_bucket_permission.uid)
        assert len(bucket_permissions) == 1
        bucket_permission = bucket_permissions[0]
        assert bucket_permission.uid == random_bucket_permission.uid
        assert bucket_permission.bucket_name == random_bucket_permission.bucket_name
        assert bucket_permission.permissions == random_bucket_permission.permissions

    @pytest.mark.asyncio
    async def test_get_read_bucket_permissions_by_uid(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting only 'READ' bucket permissions for a specific user from the CRUD Repository..

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, permission=BucketPermissionDB.Permission.WRITE
        )
        bucket_permissions = await CRUDBucketPermission.list(
            db=db, uid=random_second_user.user.uid, permission_types=[BucketPermissionDB.Permission.READ]
        )
        assert len(bucket_permissions) == 0

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_uid(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'from' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, from_=datetime.now() + timedelta(weeks=1)
        )

        bucket_permissions = await CRUDBucketPermission.list(
            db=db, uid=random_second_user.user.uid, permission_status=CRUDBucketPermission.PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 0


class TestBucketPermissionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_bucket_permissions_for_unknown_user(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for creating a bucket permission for an unknown user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowmdb.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionSchema(bucket_name=random_bucket.name, uid=uuid4())
        with pytest.raises(KeyError):
            await CRUDBucketPermission.create(permission, db=db)

    @pytest.mark.asyncio
    async def test_create_bucket_permissions_for_owner(
        self, db: AsyncSession, random_user: UserWithAuthHeader, random_bucket: Bucket
    ) -> None:
        """
        Test for creating a bucket permission for the owner of the bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing who is owner of the bucket.
        random_bucket : clowmdb.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionSchema(bucket_name=random_bucket.name, uid=random_user.user.uid)
        with pytest.raises(ValueError):
            await CRUDBucketPermission.create(permission, db=db)

    @pytest.mark.asyncio
    async def test_create_duplicate_bucket_permissions(
        self, db: AsyncSession, random_bucket_permission_schema: BucketPermissionSchema
    ) -> None:
        """
        Test for creating a duplicated bucket permission.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission_schema : app.schemas.bucket_permission.BucketPermissionOut
            Bucket permission for a random bucket for testing.
        """
        permission = BucketPermissionSchema(
            bucket_name=random_bucket_permission_schema.bucket_name, uid=random_bucket_permission_schema.uid
        )
        with pytest.raises(DuplicateError):
            await CRUDBucketPermission.create(permission, db=db)

    @pytest.mark.asyncio
    async def test_create_valid_bucket_permissions(
        self, db: AsyncSession, random_second_user: UserWithAuthHeader, random_bucket: Bucket
    ) -> None:
        """
        Test for creating a valid bucket permission.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        random_bucket : clowmdb.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionSchema(bucket_name=random_bucket.name, uid=random_second_user.user.uid)
        created_permission = await CRUDBucketPermission.create(permission, db=db)

        assert created_permission.uid == random_second_user.user.uid
        assert created_permission.bucket_name == random_bucket.name


class TestBucketPermissionCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_bucket_permissions(
        self, db: AsyncSession, random_bucket_permission: BucketPermissionDB
    ) -> None:
        """
        Test for creating a valid bucket permission.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowmdb.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        await CRUDBucketPermission.delete(
            db=db, bucket_name=random_bucket_permission.bucket_name, uid=random_bucket_permission.uid
        )

        stmt = select(BucketPermissionDB).where(
            and_(
                BucketPermissionDB.bucket_name == random_bucket_permission.bucket_name,
                BucketPermissionDB.uid_bytes == random_bucket_permission.uid.bytes,
            )
        )
        bucket_permission_db = await db.scalar(stmt)

        assert bucket_permission_db is None


class TestBucketPermissionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_bucket_permissions(
        self, db: AsyncSession, random_bucket_permission: BucketPermissionDB
    ) -> None:
        """
        Test for updating a valid bucket permission in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowmdb.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        new_from_time = round(datetime(2022, 1, 1, 0, 0).timestamp())
        new_params = BucketPermissionParametersSchema(
            from_timestamp=new_from_time,
            to_timestamp=new_from_time + 86400,  # plus one day
            permission=BucketPermissionDB.Permission.READWRITE,
            file_prefix="pseudo/folder/",
        )
        new_permission = await CRUDBucketPermission.update_permission(random_bucket_permission, new_params, db=db)

        assert new_permission.uid == random_bucket_permission.uid
        assert new_permission.bucket_name == random_bucket_permission.bucket_name
        assert new_permission.from_ == new_params.from_timestamp
        assert new_permission.to == new_params.to_timestamp
        assert new_permission.permissions == new_params.permission
        assert new_permission.file_prefix == new_params.file_prefix

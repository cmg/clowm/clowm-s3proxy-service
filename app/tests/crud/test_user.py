from uuid import uuid4

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDUser
from app.tests.utils.user import UserWithAuthHeader


class TestUserCRUD:
    @pytest.mark.asyncio
    async def test_get_user_by_id(self, db: AsyncSession, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting a user by id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowmdb.models.User
            Random user for testing.
        """
        user = await CRUDUser.get(random_user.user.uid, db=db)
        assert user
        assert random_user.user.uid == user.uid
        assert random_user.user.display_name == user.display_name

    @pytest.mark.asyncio
    async def test_get_unknown_user_by_id(
        self,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting an unknown user by id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        user = await CRUDUser.get(uuid4(), db=db)
        assert user is None

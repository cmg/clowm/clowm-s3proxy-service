import pytest
from pydantic import ValidationError

from app.schemas.bucket import BucketIn
from app.tests.utils.utils import random_ipv4_string, random_lower_string


class TestBucketName:
    def test_valid_name(self) -> None:
        """
        Test bucket schema with a valid name.
        """
        bucket = BucketIn(name=random_lower_string(), description=random_lower_string(130))
        assert bucket

    def test_ip_name(self) -> None:
        """
        Test bucket schema with an invalid name.\n
        100 random IPv4 addresses will be evaluated.
        """
        desc = random_lower_string(130)
        for _ in range(100):
            with pytest.raises(ValidationError):
                BucketIn(name=random_ipv4_string(), description=desc)

    def test_too_short_name(self) -> None:
        """
        Test bucket schema with a too short name.
        """
        with pytest.raises(ValidationError):
            BucketIn(name=random_lower_string(2), description=random_lower_string(130))

    def test_too_long_name(self) -> None:
        """
        Test bucket schema with a too long name.
        """
        with pytest.raises(ValidationError):
            BucketIn(name=random_lower_string(64), description=random_lower_string(130))

    def test_name_with_tags(self) -> None:
        """
        Test bucket schema with a name with tags.
        """
        name = f"{random_lower_string(10)}.{random_lower_string(10)}.{random_lower_string(10)}.{random_lower_string(9)}"
        bucket = BucketIn(name=name, description=random_lower_string(130))
        assert bucket

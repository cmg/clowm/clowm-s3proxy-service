import re

from pydantic import BaseModel, ConfigDict, Field, field_validator

from app.schemas import UUID

ip_like_regex = re.compile(r"^(\d+\.){3}\d+$")


class _BaseBucket(BaseModel):
    """
    Base Schema for a bucket.
    """

    name: str = Field(
        ...,
        examples=["test-bucket"],
        description="Name of the bucket",
        min_length=3,
        max_length=63,
        pattern=r"^([a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)*[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$",
        # https://docs.ceph.com/en/latest/radosgw/s3/bucketops/#constraints
    )
    description: str = Field(
        ...,
        examples=["This is a sample description of a bucket"],
        description="Description of the bucket",
        min_length=16,
        max_length=2**16,
    )

    @field_validator("name")
    @classmethod
    def name_is_not_an_ip_address(cls, name: str) -> str:
        if ip_like_regex.search(name):
            raise ValueError("no IP address as bucket name")
        return name


class BucketIn(_BaseBucket):
    """
    Schema for creating a new bucket.
    """


class BucketSizeLimits(BaseModel):
    size_limit: int | None = Field(
        None, gt=0, lt=2**32, description="Size limit of the bucket in KiB", examples=[10240]
    )
    object_limit: int | None = Field(
        None, gt=0, lt=2**32, description="Number of objects limit of the bucket", examples=[10000]
    )


class BucketOut(_BaseBucket, BucketSizeLimits):
    """
    Schema for answering a request with a bucket.
    """

    created_at: int = Field(
        ...,
        examples=[1640991600],  # 01.01.2022 00:00
        description="Time when the bucket was created as UNIX timestamp",
    )
    owner_id: UUID = Field(..., description="UID of the owner", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"])
    public: bool = Field(..., description="Flag if the bucket is anonymously readable")
    model_config = ConfigDict(from_attributes=True)

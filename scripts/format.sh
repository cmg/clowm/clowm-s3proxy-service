#!/bin/sh -e
set -x

ruff format app
ruff check --fix --show-fixes app
isort app

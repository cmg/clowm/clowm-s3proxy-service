#! /usr/bin/env bash

# Check Connection to Ceph RGW
python app/check_ceph_connection.py
# Let the DB start
python app/check_database_connection.py
